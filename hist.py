import io
import json
import logging
import os
from collections import defaultdict
from datetime import datetime
from ROOT import (TCanvas, TPad, TFile, TPaveLabel,
                  TPaveText, gROOT, TH1F, TH1D, TLegend,
                  gStyle, TH2F, TChain, TGraphErrors, TText, gPad, gROOT)
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.colors import LogNorm

plt.style.use('seaborn-paper')

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)


def plot():
    f = TFile.Open("/Users/ric/Documents/DPhil/RAL/"
                   "Dosimetry/beamSpot_180823.root")
    map = getattr(f, 'Dose Map')
    X, Y = [], []

    print(map.GetSize())

    x_hist = map.ProjectionX()
    x_hist.Rebin()
    y_hist = map.ProjectionY()

    for i in range(x_hist.GetNbinsX()):
        X.append(x_hist.GetBinContent(i))
    for i in range(y_hist.GetNbinsX()):
        Y.append(y_hist.GetBinContent(i))

    print(len(X), len(Y))
    plt.hist2d(X, Y, bins=40, norm=LogNorm())
    plt.colorbar()
    plt.show()


if __name__ == '__main__':
    plot()
