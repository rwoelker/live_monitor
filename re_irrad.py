from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import io
import logging
import os
from collections import defaultdict
from datetime import datetime

import matplotlib.pyplot as plt
import numpy as np
from matplotlib.lines import Line2D

plt.style.use('seaborn-dark-palette')
plt.rcParams['font.family'] = 'serif'
plt.rcParams['font.serif'] = 'Ubuntu'
plt.rcParams['font.monospace'] = 'Ubuntu Mono'
plt.rcParams['font.size'] = 10
plt.rcParams['axes.labelsize'] = 10
plt.rcParams['axes.labelweight'] = 'bold'
plt.rcParams['xtick.labelsize'] = 8
plt.rcParams['ytick.labelsize'] = 8
plt.rcParams['legend.fontsize'] = 10
plt.rcParams['figure.titlesize'] = 12

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)


def makedir(path):
    if not os.path.exists(os.path.dirname(path)):
        os.makedirs(os.path.dirname(path))


def file_list(
        dir='/Users/rodriguez/Documents/Misc_Documents/DPhil/RAL/irrad_data/2018-07-18/Irradiations/data/'):
    _list = []
    for root, dirs, fls in os.walk(dir):
        for f in fls:
            if f == "Monitoring.txt":
                _list.append(os.path.join(root, f))
    return _list


def get_data(filename):
    dat = defaultdict(list)
    with io.open(filename, 'r') as f:
        for index, l in enumerate(f):
            if l.startswith('==='):
                dt_string = l.split('===')[1].strip()
                dt_obj = datetime.strptime(dt_string, '%d/%m/%Y %H:%M.%S')
                dat['dt'].append(dt_obj)
            elif l.startswith('NTC'):
                dat['t'].append(float(l.split('=')[1].strip()))
            elif l.startswith('IDDD'):
                dat['i'].append(float(l.split('=')[1].strip()))
            elif l.startswith('VDDA_RAW'):
                dat['vdda'].append(float(l.split('=')[2].strip()))
            elif l.startswith('VDDD_RAW'):
                dat['vddd'].append(float(l.split('=')[2].strip()))
    dat['name'] = filename
    return dat


def plot_dose(files):
    custom_lines = [Line2D([0], [0], color='b', lw=4),
                    Line2D([0], [0], color='k', lw=4)]
    fig = plt.figure(figsize=(6, 4))
    ax = fig.add_subplot(1, 1, 1)
    ax.set_ylabel(r'current-increase ratio')
    ax.set_xlabel(r'dose [Mrad]')
    ax.set_xlim(0, 12)
    ax.set_ylim(1, 5.6)
    first_files, second_files = [], []
    for f in files:
        if f.endswith('_2/Monitoring.txt'):
            second_files.append(f)
        else:
            first_files.append(f)
    for i, (f, s) in enumerate(zip(first_files, second_files)):

        if i + 1 != 1 and i + 1 != 3:
            continue
        print(f)
        c = {1: 'k', 2: 'b', 3: 'm', 4: 'g'}[i + 1]
        ls = '-' if i + 1 == 1 else '--'
        dat = get_data(f)
        start = dat['i'][0]
        startdate = dat['dt'][0]
        dts = [(p - startdate).total_seconds() for p in dat['dt']]
        X_1 = np.array(dts) * 0.78 / 3600
        Y_1 = np.array(dat['i']) / start
        # if i + 1 == 1:
        #     print(X_1[:10])
        #     X_1 -= 4
        #     print(X_1[:10])
        ax.plot(X_1, Y_1, color='k', linewidth=1, linestyle=ls)

        dat_2 = get_data(s)
        start_2 = dat_2['i'][0]
        startdate_2 = dat_2['dt'][0]
        dts = [(p - startdate_2).total_seconds() for p in dat_2['dt']]
        X_2 = X_1[-1] + np.array(dts) * 0.78 / 3600
        Y_2 = np.array(dat_2['i']) / start_2
        ax.plot(X_2, Y_2, color='k', linewidth=1, linestyle=ls)

    # leg1 = ax.legend(loc=1)
    # fig.gca().add_artist(leg1)
    path = os.path.join('plots/re_irrad_joined/{}/'.format(
        datetime.today().strftime('%Y-%m-%d')),
        'joined_re_irrad_both.pdf')
    makedir(path)
    fig.savefig(path)
    logger.info('Saved plot under {}'.format(path))


if __name__ == '__main__':
    files = sorted(file_list())
    chips = ['VRCQ26H', 'VCCQ70H', 'AYQ2XTH']
    fls = [f for f in files if any(c in f for c in chips)]
    print(len(fls))
    for f in fls[-8:]:
        print('===', f)
    plot_dose(fls[-8:])
