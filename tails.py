from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import io
import json
import logging
import os
from datetime import datetime

import matplotlib.pyplot as plt
import numpy as np
from matplotlib.lines import Line2D
from scipy.stats import linregress

plt.style.use('seaborn-paper')

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)

with io.open('annealing.json', 'r') as f:
    metadata = json.loads(f.read())


def read_dat(fname):
    with io.open(fname, 'r') as f:
        lines = f.read().split('\n')
    currents = []
    for line in lines:
        if line.lower().startswith('iddd'):
            currents.append(float(line.split('=')[-1]))
    return currents


def get_tail_first_irrad(fname):
    data = read_dat(fname)
    return np.mean(data[-3:])


def get_peak_second_irrad(fname):
    data = read_dat(fname)
    return np.amax(data)


def file_list(dir, fnames):
    first_irrads = []
    second_irrads = []
    second_names = [n + "_2" for n in fnames]
    for root, dirs, fls in os.walk(dir):
        for f in fls:
            if os.path.basename(root) in fnames:
                if f == "Monitoring.txt":
                    first_irrads.append(os.path.join(root, f))
            elif os.path.basename(root) in second_names:
                if f == "Monitoring.txt":
                    second_irrads.append(os.path.join(root, f))
    return sorted(first_irrads), sorted(second_irrads)


def get_annealing_time(meta, name):
    for d in meta:
        if d["name"] == name:
            return d["annealing"]
    raise Exception


def make_data(meta, path):
    fnames = [d["name"] for d in meta]
    first_list, second_list = file_list(path, fnames)
    out = []
    for f, s in zip(first_list, second_list):
        tail = get_tail_first_irrad(f)
        peak = get_peak_second_irrad(s)
        chipname = os.path.basename(os.path.dirname(f))
        _chipname = os.path.basename(os.path.dirname(s))
        if chipname + "_2" != _chipname:
            raise Exception("ERROR!!!")
        annealing = get_annealing_time(meta, chipname)
        high_t = (annealing == 60 or annealing == 120)

        d = {
            "name": chipname,
            "tail": tail,
            "peak": peak,
            "high_T": high_t,
            "annealing": annealing
        }
        out.append(d)

    return out


def makedir(path):
    if not os.path.exists(os.path.dirname(path)):
        os.makedirs(os.path.dirname(path))


def plot_peaks(data):
    fig = plt.figure(figsize=(8, 6))

    ax1 = fig.add_subplot(1, 1, 1)
    ax1.set_xlim(0, 330)
    ax1.set_ylim(0.5, 1.5)

    ax1.set_xlabel('curing time [d]', fontsize=16)
    ax1.set_ylabel('current-increase peak-tail ratio', fontsize=16)

    custom_patches = [
        Line2D([0], [0], color='w', marker='^', markerfacecolor=(0, 1, 1, 1),
               markersize=12),
        Line2D([0], [0], color='w', marker='^',
               markerfacecolor=(1.0, 0.0, 1.0, 1.0),
               markersize=12)]
    ax1.legend(custom_patches, [r'$20~^\circ$C', r'$80~^\circ$C'],
               loc="upper right", fontsize=14)
    X, Y = [], []
    for i, dat in enumerate(data):
        x = dat["annealing"]
        X.append(x)
        y = dat["peak"] / dat["tail"]
        Y.append(y)
        color = (0, 1, 1, 1) if not dat['high_T'] else (1.0, 0.0, 1.0, 1.0)
        ax1.scatter(x, y, c=color, marker='^', s=120)

    print(json.dumps([d for d in data if d["annealing"] == 120], indent=2))
    ax1.plot([0, 330], [1, 1], color='black', linestyle='--', linewidth=0.5)
    slope, intercept, r_value, p_value, std_err = linregress(X, Y)
    logger.info('slope: {:.3g}, intercept: {:.3g}, '
                'r_value: {:.3g}, p_value: {:.3g}, '
                'std_err: {:.3g}'.format(slope, intercept, r_value, p_value,
                                         std_err))
    path = 'plots/re_irradiations/{}_tails.pdf'.format(
        datetime.today().strftime('%Y-%m-%d'))
    plt.xticks(fontsize=14)
    plt.yticks(fontsize=14)
    makedir(path)
    fig.savefig(path)
    logger.info('Saved plot under {}'.format(path))


if __name__ == '__main__':
    data = make_data(
        metadata,
        ('/Users/ric/Documents/DPhil/RAL/'
         'irrad_data/2018-08-31/Irradiations/data')
    )

    plot_peaks(data)
