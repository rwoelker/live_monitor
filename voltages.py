from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import io
import logging
import os
from collections import defaultdict
from datetime import datetime, timedelta

import matplotlib.pyplot as plt
import numpy as np
from matplotlib.lines import Line2D
from scipy.signal import medfilt
from scipy.stats import norm, linregress

plt.style.use('seaborn-dark-palette')
plt.rcParams['font.family'] = 'serif'
plt.rcParams['font.serif'] = 'Ubuntu'
plt.rcParams['font.monospace'] = 'Ubuntu Mono'
plt.rcParams['font.size'] = 10
plt.rcParams['axes.labelsize'] = 10
plt.rcParams['axes.labelweight'] = 'bold'
plt.rcParams['xtick.labelsize'] = 8
plt.rcParams['ytick.labelsize'] = 8
plt.rcParams['legend.fontsize'] = 10
plt.rcParams['figure.titlesize'] = 12

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)


def makedir(path):
    if not os.path.exists(os.path.dirname(path)):
        os.makedirs(os.path.dirname(path))


def file_list(
        dir='/Users/rodriguez/Documents/Misc_Documents/DPhil/RAL/irrad_data/2018-04-13/Irradiations/data/'):
    file_list = []
    for root, dirs, fls in os.walk(dir):
        for f in fls:
            if f == "Monitoring.txt":
                file_list.append(os.path.join(root, f))
    return file_list


def sorted_data(files, new_tube_only=True,
                old_tube_only=False):  # sorted by datetime
    data = [get_data(f) for f in files]
    if new_tube_only:
        _data = []
        for d in data:
            if d['dt'][0] >= datetime(2018, 2, 9):
                _data.append(d)
        return sorted(_data, key=lambda x: x['dt'][0])
    elif old_tube_only:
        _data = []
        for d in data:
            if d['dt'][0] < datetime(2018, 2, 9):
                _data.append(d)
        return sorted(_data, key=lambda x: x['dt'][0])

    return sorted(data, key=lambda x: x['dt'][0])


def get_data(filename):
    dat = defaultdict(list)
    with io.open(filename, 'r') as f:
        for index, l in enumerate(f):
            if l.startswith('==='):
                dt_string = l.split('===')[1].strip()
                dt_obj = datetime.strptime(dt_string, '%d/%m/%Y %H:%M.%S')
                dat['dt'].append(dt_obj)
            elif l.startswith('NTC'):
                dat['t'].append(float(l.split('=')[1].strip()))
            elif l.startswith('IDDD'):
                dat['i'].append(float(l.split('=')[1].strip()))
            elif l.startswith('VDDA_RAW'):
                dat['vdda'].append(float(l.split('=')[2].strip()))
            elif l.startswith('VDDD_RAW'):
                dat['vddd'].append(float(l.split('=')[2].strip()))
    dat['name'] = filename
    return dat


def plot_tid_occurence(files):
    old_data = sorted_data(files, old_tube_only=True, new_tube_only=False)
    first_one = old_data[0]['dt'][0]
    X, Y = [], []
    for dat in old_data:
        start_time = dat['dt'][0]
        idx_at_max = np.argmax([d / dat['i'][0] for d in dat['i']])
        time_at_max = dat['dt'][idx_at_max]
        x = (start_time - first_one).days + 1
        X.append(x)
        y = (time_at_max - start_time).seconds / 60
        if y < 100:
            Y.append(y)
    old_mean = np.mean(Y)
    logger.info('Old array: {}'.format(Y))
    logger.info("{}, {}".format(np.mean(Y), np.std(Y)))

    data = sorted_data(files)
    fig = plt.figure(figsize=(10, 6))

    ax1 = fig.add_subplot(1, 1, 1)
    ax1.set_xlim(0, 200)
    ax1.set_ylim(30, 70)

    ax1.plot([0, 300], [old_mean, old_mean], linestyle='--', color='blue')

    ax1.set_xlabel('hours of operation')
    ax1.set_ylabel('occurrence of TID peak after start [mins]')
    first_one = data[0]['dt'][0]
    logger.info("first one: {}".format(first_one))
    X, Y = [], []
    for i, dat in enumerate(data):
        start_time = dat['dt'][0]
        idx_at_max = np.argmax([d / dat['i'][0] for d in dat['i']])
        time_at_max = dat['dt'][idx_at_max]
        x = i * 5
        print(x)
        X.append(x)
        y = (time_at_max - start_time).seconds / 60
        Y.append(y)
        ax1.scatter(x, y, c='k', marker='x', s=40)
    logger.info('New array: {}'.format(Y))
    logger.info("{}, {}".format(np.mean(Y), np.std(Y)))

    slope, intercept, r_value, p_value, std_err = linregress(X, Y)
    logger.info('slope: {:.3g}, intercept: {:.3g}, '
                'r_value: {:.3g}, p_value: {:.3g}, '
                'std_err: {:.3g}'.format(slope, intercept, r_value, p_value,
                                         std_err))

    X_fit = np.array([0, 300])
    Y_fit = slope * X_fit + intercept
    ax1.plot(X_fit, Y_fit, linestyle='solid', color='black')

    path = 'plots/tid_occurrence/{}_tid_occurrence.pdf'.format(
        datetime.today().strftime('%Y-%m-%d'))
    logger.info('Have processed {} points'.format(len(data)))
    makedir(path)
    fig.savefig(path)
    logger.info('Saved plot under {}'.format(path))


def plot_gaussians(files, mode):
    limits = (-60, 60)
    fig = plt.figure(figsize=(10, 6))

    ax1 = fig.add_subplot(1, 1, 1)

    ax1.set_xlabel(
        'VDDD deviation from the mean [mV]' if mode == 'vddd' else 'VDDA deviation from the mean [mV]')
    ax1.set_ylabel('frequency (a.u.)')
    ax1.set_xlim(limits[0], limits[1])
    # ax1.set_ylim(1100, 1300)

    for f in files:
        logger.info("Processing file {}".format(f))
        dat = get_data(f)
        stdev = np.std(dat['vddd'] if mode == 'vddd' else dat['vdda'])
        x = np.linspace(limits[0], limits[1], 1000)
        ax1.plot(x, norm.pdf(x, 0, stdev), linewidth=0.05)

    path = 'plots/voltages_gaussians/{}_{}_gaussians.pdf'.format(
        datetime.today().strftime('%Y-%m-%d'), mode)

    makedir(path)
    fig.savefig(path)
    logger.info('Saved plot under {}'.format(path))


def plot_voltages(files, mode, filter):
    fig = plt.figure(figsize=(10, 6))

    ax1 = fig.add_subplot(1, 1, 1)

    ax1.set_xlabel('TID [Mrad]')
    ax1.set_ylabel('VDDD [mV]' if mode == 'vddd' else 'VDDA [mV]')

    ax1.set_xlim(0, 4)
    ax1.set_ylim(1100, 1300)

    for f in files:
        logger.info("Processing file {}".format(f))
        dat = get_data(f)
        start_time = dat['dt'][0]
        doses = [(d - start_time).total_seconds() * 0.78 / 3600 for d in
                 dat['dt']]
        if filter:
            ax1.plot(doses,
                     medfilt(dat['vddd'], 5) if mode == 'vddd' else medfilt(
                         dat['vdda'], 5),
                     linewidth=0.01)
        else:
            ax1.plot(doses,
                     dat['vddd'] if mode == 'vddd' else dat['vdda'],
                     linewidth=0.01)

    path = 'plots/voltages{}/{}_{}{}.pdf'.format('_filtered' if filter else '',
                                                 datetime.today().strftime(
                                                     '%Y-%m-%d'),
                                                 mode,
                                                 '_filtered' if filter else '')

    makedir(path)
    fig.savefig(path)
    logger.info('Saved plot under {}'.format(path))


def plot_temp(files):
    custom_lines = [Line2D([0], [0], color='b', lw=4),
                    Line2D([0], [0], color='k', lw=4)]
    STYLES = {
        'initial_temp': ('green', 'D'),
        'final_temp': ('blue', 'x'),
        'mean_temp': ('magenta', '.')
    }
    fig = plt.figure(figsize=(10, 6))
    ax = fig.add_subplot(1, 1, 1)
    ax.set_ylabel(r'TID peak')
    ax.set_xlabel(r'temperature [C]')
    ax.set_xlim(12.5, 25)
    ax.set_ylim(3, 9)
    for t in ['mean_temp', 'initial_temp']:
        for i, f in enumerate(files):
            if "_065" not in f and "_63" not in f and "_067" not in f:
                continue
            elif "A6Q2XKH_065" in f:
                continue
            dat = get_data(f)
            if t == "mean_temp":
                x = np.mean(dat['t'])
            else:
                x = np.mean(dat['t'][10])
            y = np.amax(np.array(dat['i'])[1:] / dat['i'][0])
            markersize = 8
            color = 'b' if "data/A" in f else 'k'
            if t == 'mean_temp' and i == 0:
                label = 'mean'
                # elif t == 'final_temp' and i == 0:
                # label = 'final'
            elif t == 'initial_temp' and i == 0:
                label = 'initial'
            else:
                label = None
            ax.plot(x, y, c=color, marker=STYLES[t][1], markersize=8,
                    linestyle=':', label=label)

    leg1 = ax.legend(loc=1)
    ax.legend(custom_lines, ['batch 2', 'batch 3'], loc="upper center")
    fig.gca().add_artist(leg1)
    path = os.path.join(
        'plots/temperature/{}/'.format(datetime.today().strftime('%Y-%m-%d')),
        'temperature.pdf')
    makedir(path)
    fig.savefig(path)
    logger.info('Saved plot under {}'.format(path))


def plot_dose(files):
    custom_lines = [Line2D([0], [0], color='b', lw=4),
                    Line2D([0], [0], color='k', lw=4)]
    STYLES = {
        'initial_temp': ('green', 'D'),
        'final_temp': ('blue', 'x'),
        'mean_temp': ('magenta', '.')
    }
    fig = plt.figure(figsize=(10, 6))
    ax = fig.add_subplot(1, 1, 1)
    ax.set_ylabel(r'TID peak')
    ax.set_xlabel(r'dose rate [krad/h]')
    ax.set_xlim(12.5, 25)
    ax.set_ylim(3, 9)
    for t in ['mean_temp', 'initial_temp']:
        for i, f in enumerate(files):
            if "_065" not in f and "_63" not in f and "_067" not in f:
                continue
            elif "A6Q2XKH_065" in f:
                continue
            dat = get_data(f)
            if t == "mean_temp":
                x = np.mean(dat['t'])
            else:
                x = np.mean(dat['t'][10])
            y = np.amax(np.array(dat['i'])[1:] / dat['i'][0])
            markersize = 8
            color = 'b' if "data/A" in f else 'k'
            if t == 'mean_temp' and i == 0:
                label = 'mean'
                # elif t == 'final_temp' and i == 0:
                # label = 'final'
            elif t == 'initial_temp' and i == 0:
                label = 'initial'
            else:
                label = None
            ax.plot(x, y, c=color, marker=STYLES[t][1], markersize=8,
                    linestyle=':', label=label)

    leg1 = ax.legend(loc=1)
    ax.legend(custom_lines, ['batch 2', 'batch 3'], loc="upper center")
    fig.gca().add_artist(leg1)
    path = os.path.join(
        'plots/temperature/{}/'.format(datetime.today().strftime('%Y-%m-%d')),
        'temperature.pdf')
    makedir(path)
    fig.savefig(path)
    logger.info('Saved plot under {}'.format(path))


if __name__ == '__main__':
    files = file_list()
    # plot_voltages(files, mode='vdda', filter=True)
    # plot_gaussians(files, mode='vdda')
    plot_tid_occurence(files)
    # plot_temp(files)
