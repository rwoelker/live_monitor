import io
import logging
import os
from collections import defaultdict
from datetime import datetime

import matplotlib.pyplot as plt
import numpy as np

plt.style.use('seaborn-paper')
logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)


def makedir(path):
    if not os.path.exists(os.path.dirname(path)):
        os.makedirs(os.path.dirname(path))


def file_list(dir=('/Users/ric/Documents/DPhil/RAL'
                   '/irrad_data/2018-08-31/Irradiations/data2/')):
    file_list = []
    for root, dirs, fls in os.walk(dir):
        for f in fls:
            if f == "Monitoring.txt":
                file_list.append(os.path.join(root, f))
    return file_list


def sorted_data(files, new_tube_only=True):  # sorted by datetime
    data = [get_data(f) for f in files]
    if new_tube_only:
        _data = []
        for d in data:
            if d['dt'][0] >= datetime(2018, 2, 9):
                _data.append(d)
        return sorted(_data, key=lambda x: x['dt'][0])

    return sorted(data, key=lambda x: x['dt'][0])


def get_data(filename):
    dat = defaultdict(list)
    with io.open(filename, 'r') as f:
        for index, l in enumerate(f):
            if l.startswith('==='):
                dt_string = l.split('===')[1].strip()
                dt_obj = datetime.strptime(dt_string, '%d/%m/%Y %H:%M.%S')
                dat['dt'].append(dt_obj)
            elif l.startswith('IDDD'):
                dat['i'].append(float(l.split('=')[1].strip()))
    dat['name'] = filename
    return dat


def plot_tid_occurence(files):
    data = sorted_data(files)
    fig = plt.figure(figsize=(8, 6))

    ax1 = fig.add_subplot(1, 1, 1)
    ax1.set_xlim(0, 1.5)
    # ax1.set_ylim(30, 70)

    ax1.set_xlabel('accumulated dose at maximum CIR [Mrad]', fontsize=16)
    ax1.set_ylabel('count', fontsize=16)
    first_one = data[0]['dt'][0]
    logger.info("first one: {}".format(first_one))
    X = []
    for i, dat in enumerate(data):
        start_time = dat['dt'][0]
        idx_at_max = np.argmax([d / dat['i'][0] for d in dat['i']])
        time_at_max = dat['dt'][idx_at_max]
        dt = (time_at_max - start_time)
        dose_at_max = dt.total_seconds() / 3600 * .78
        logger.debug('dose at max', dose_at_max)
        print('measurement', i)
        if dose_at_max < 2:
            X.append(dose_at_max)

    ax1.hist(X, range=(0, 5), bins=100)
    logger.info('Mean dose at peak {:.3g} +/- {:.3g}'
                ''.format(np.mean(X), np.std(X)))

    # ax1.text(.8, 15,
    #          'mean dose ' + r'$={:.2f} \pm {:.2f}$'.format(np.mean(X),
    #                                                        np.std(X)),
    #          fontsize=12)

    path = 'plots/time_at_tid/{}_time_at_tid.pdf'.format(
        datetime.today().strftime('%Y-%m-%d'))

    plt.xticks(fontsize=14)
    plt.yticks(fontsize=14)
    logger.info('Have processed {} points'.format(len(data)))
    makedir(path)
    fig.savefig(path)
    logger.info('Saved plot under {}'.format(path))


if __name__ == '__main__':
    files = file_list()
    plot_tid_occurence(files)
