from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import io
import logging
import os
from collections import defaultdict
from datetime import datetime

import matplotlib as mpl
import matplotlib.cm as cmx
import matplotlib.colors as colors
import matplotlib.pyplot as plt
import numpy as np
from matplotlib import gridspec

plt.style.use('seaborn-dark-palette')
plt.rcParams['font.family'] = 'serif'
plt.rcParams['font.serif'] = 'Ubuntu'
plt.rcParams['font.monospace'] = 'Ubuntu Mono'
plt.rcParams['font.size'] = 10
plt.rcParams['axes.labelsize'] = 10
plt.rcParams['axes.labelweight'] = 'bold'
plt.rcParams['xtick.labelsize'] = 8
plt.rcParams['ytick.labelsize'] = 8
plt.rcParams['legend.fontsize'] = 10
plt.rcParams['figure.titlesize'] = 12

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)

# {chip_no: (r, theta, peak_2 (A), peak_3 (V), peak_4)}
_chips = {
    "005": (1, np.pi / 2., 6.1208, 5.0473),
    "024": (0.8, 3.5 * np.pi / 4., None, 4.9549),
    "026": (0.7, 3 * np.pi / 4., 6.4268, None),
    "034": (0.7, np.pi / 4., 7.1019, 5.4604),
    "057": (1, np.pi, 5.9405, 4.1231),
    "065": (0., 0., 7.3217, 5.647),
    "073": (1., 0., 5.5232, 5.9168),
    "096": (0.7, 5 * np.pi / 4, 6.6799, 4.4554),
    "104": (0.7, 7 * np.pi / 4, 6.4586, 5.9955),
    "130": (1, 3 * np.pi / 2, 5.5504, 4.3153),
}

norm_ratio = _chips["065"][2] / _chips["065"][3]

chips = {}
for k, v in _chips.items():
    if v[3] is not None:
        v = (v[0], v[1], v[2], v[3] * norm_ratio)
    chips[k] = v

peaks_2 = [v[2] for _, v in chips.items() if v[2] is not None]
peaks_3 = [v[3] for _, v in chips.items() if v[3] is not None]


def trans_polar(r=0, theta=0, x0=0, y0=0):
    x = r * np.cos(theta)
    y = r * np.sin(theta)
    x1 = x + x0
    y1 = y + y0
    r1 = np.sqrt(x1 ** 2 + y1 ** 2)
    t1 = np.arccos((x + x0) / r1)
    if theta >= np.pi:
        t1 = -t1
    return t1, r1


def makedir(path):
    if not os.path.exists(os.path.dirname(path)):
        os.makedirs(os.path.dirname(path))


def file_list(
        dir='/Users/rodriguez/Documents/Misc_Documents/DPhil/RAL/irrad_data/2018-04-06/Irradiations/data/'):
    file_list = []
    for root, dirs, fls in os.walk(dir):
        for f in fls:
            if f == "Monitoring.txt":
                file_list.append(os.path.join(root, f))
    return file_list


def sorted_data(files, new_tube_only=True):  # sorted by datetime
    data = [get_data(f) for f in files]
    if new_tube_only:
        _data = []
        for d in data:
            if d['dt'][0] >= datetime(2018, 2, 9):
                _data.append(d)
        return sorted(_data, key=lambda x: x['dt'][0])

    return sorted(data, key=lambda x: x['dt'][0])


def get_data(filename):
    dat = defaultdict(list)
    with io.open(filename, 'r') as f:
        for index, l in enumerate(f):
            if l.startswith('==='):
                dt_string = l.split('===')[1].strip()
                dt_obj = datetime.strptime(dt_string, '%d/%m/%Y %H:%M.%S')
                dat['dt'].append(dt_obj)
            elif l.startswith('NTC'):
                dat['t'].append(float(l.split('=')[1].strip()))
            elif l.startswith('IDDD'):
                dat['i'].append(float(l.split('=')[1].strip()))
            elif l.startswith('VDDA_RAW'):
                dat['vdda'].append(float(l.split('=')[2].strip()))
            elif l.startswith('VDDD_RAW'):
                dat['vddd'].append(float(l.split('=')[2].strip()))
    return dat


def plot_geo(files):
    fig = plt.figure(figsize=(10, 6))

    gs1 = gridspec.GridSpec(2, 2, height_ratios=[20, 1])
    # gs1.update(left=0.05, right=0.48, wspace=0.05)

    ax1 = plt.subplot(gs1[0, 0], projection='polar')
    ax2 = plt.subplot(gs1[1, :])
    ax3 = plt.subplot(gs1[0, 1], projection='polar')

    ax1.set_title('Batch 2: A9Q8IIH')
    ax3.set_title('Batch 3: VXCQ20H')
    ax3.text(0.5, -0.05, 'scaled up by factor of {:.2f}\n'
                         '(normalised to chip 65 from batch 2)'.format(
        norm_ratio),
             ha='center', va='center', transform=ax3.transAxes, fontsize=8)

    for a in (ax1, ax3):
        a.grid(False)
        a.set_rticks([])
        a.set_xticks([])
        a.set_rmax(1)
        a.set_ylim(0, 1.15)
        a.set_facecolor('gainsboro')

    vmin = np.floor(np.amin(peaks_2 + peaks_3))
    vmax = np.ceil(np.amax(peaks_2 + peaks_3))

    hot = cm = mpl.cm.viridis
    cNorm = colors.Normalize(vmin=vmin - 1, vmax=vmax)
    scalarMap = cmx.ScalarMappable(norm=cNorm, cmap=hot)

    for k, v in chips.items():
        if v[2] is not None:
            ax1.scatter(v[1], v[0], marker='s', s=500,
                        color=scalarMap.to_rgba(v[2]))
            ax1.annotate(k, trans_polar(r=v[0], theta=v[1], x0=-0.05))

        if v[3] is not None:
            ax3.scatter(v[1], v[0], marker='s', s=500,
                        color=scalarMap.to_rgba(v[3]))
            ax3.annotate(k, trans_polar(r=v[0], theta=v[1], x0=-0.05))

    ax2.set_position([0.3, 0.12, 0.4, 0.015])
    cb1 = mpl.colorbar.ColorbarBase(ax2, cmap=hot,
                                    norm=cNorm,
                                    orientation='horizontal')
    cb1.set_label('TID peak (current-increase ratio)', labelpad=None)

    path = 'plots/geographical/{}_geographical.pdf'.format(
        datetime.today().strftime('%Y-%m-%d'))

    makedir(path)
    fig.savefig(path)
    logger.info('Saved plot under {}'.format(path))


if __name__ == '__main__':
    files = file_list()
    plot_geo(files)
