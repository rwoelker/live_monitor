import ROOT

ROOT.gStyle.SetOptStat(0)

ratio = 1

chips = [
    {'name': 'A6Q2XKH_089', 'description': 'Batch 1, FCF', 'color': 2},
    {'name': 'AYQ2XTH_029', 'description': 'Batch 1, FCF', 'color': 3},
    {'name': 'AGQ8DWH_028', 'description': 'Batch 2, non-FCF', 'color': 4},
    {'name': 'A6Q2XKH_134', 'description': 'Batch 1, non-FCF', 'color': 12},
    {'name': 'A8Q2Y0H_092', 'description': 'Batch 1, non-FCF', 'color': 7},
    {'name': 'ACQ8IFH_132', 'description': 'Batch 2, non-FCF', 'color': 6},
    {'name': 'A4Q8INH_133', 'description': 'Batch 2, FCF', 'color': 11},
    {'name': 'AGQ8DWH_027', 'description': 'Batch 2, FCF', 'color': 13},
    {'name': 'AYQ2XTH_035', 'description': 'Batch 1, FCF', 'color': 14},
    {'name': 'AYQ2XTH_077', 'description': 'Batch 1, FCF', 'color': 15},
    {'name': 'AYQ2XTH_018', 'description': 'Batch 1, FCF', 'color': 16},
    {'name': 'AYQ2XTH_037', 'description': 'Batch 1, FCF', 'color': 17},
    {'name': 'VRCQ26H_065', 'description': 'Batch 3, non-FCF', 'color': 1},
    {'name': 'ASQ8E2H_065', 'description': 'Batch 2, non-FCF', 'color': 18},
    {'name': 'A9Q8IIH_065', 'description': 'Batch 2, non-FCF', 'color': 8},
    # {'name': 'A7Q8IKH_065', 'description': 'Batch 2, non-FCF', 'color': 20},
    {'name': 'ADQ81EH_065', 'description': 'Batch 2, non-FCF', 'color': 21}
]

c1 = ROOT.TCanvas("", "", 800, 600)

dummy = ROOT.TH1F("", "", 1, 0, 8.5)
dummy.SetMaximum(0.20)
dummy.SetMinimum(0.00)
if ratio:
    dummy.SetMaximum(8.00)
    dummy.SetMinimum(0.90)
dummy.GetYaxis().SetTitle("Current Increase Ratio")
dummy.GetYaxis().SetTitleOffset(1.2)
dummy.GetXaxis().SetTitle("Dose [Mrad]")
dummy.Draw("hist")

leg = ROOT.TLegend(0.5, 0.5, 0.85, 0.8)
leg.SetBorderSize(0)

graphs = []

outf = ROOT.TFile("summary.root", "RECREATE")

for count, chip in enumerate(chips):
    if ratio:
        graphs.append(ROOT.TFile(chip['name'] + "/out.root").Get("Ratio"))
    else:
        graphs.append(ROOT.TFile(chip['name'] + "/out.root").Get("Graph"))
    if "HCC" in chip:
        graphs[-1].SetLineColor(count)
    else:
        # line style for batch
        if "Q2" in chip['name']:
            style = 7
        elif "Q8" in chip['name']:
            style = 1
        elif "V" in chip['name'][0]:
            style = 2
        # line width for FCF
        if "non-FCF" in chip['description']:
            width = 1
        else:
            width = 2
        graphs[-1].SetLineColor(chip['color'])
        graphs[-1].SetLineStyle(style)
        graphs[-1].SetLineWidth(width)
    graphs[-1].Draw("lsame")
    outf.cd()
    graphs[-1].Write(chip['name'])
    if chip['description'] == "":
        leg.AddEntry(graphs[-1], chip['name'], "l")
    else:
        leg.AddEntry(graphs[-1],
                     chip['name'] + " (" + chip['description'] + ")", "l")

leg.Draw()

if ratio:
    c1.Print("summary_ratio.pdf")
else:
    c1.Print("summary.pdf")
