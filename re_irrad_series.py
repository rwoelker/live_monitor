import io
import json
import logging
import os
from collections import defaultdict
from datetime import datetime

import matplotlib.pyplot as plt
import numpy as np
from matplotlib.lines import Line2D

plt.style.use('seaborn-paper')

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)


def get_data(filename):
    dat = defaultdict(list)
    with io.open(filename, 'r') as f:
        for index, l in enumerate(f):
            if l.startswith('==='):
                dt_string = l.split('===')[1].strip()
                dt_obj = datetime.strptime(dt_string, '%d/%m/%Y %H:%M.%S')
                dat['dt'].append(dt_obj)
            elif l.startswith('IDDD'):
                dat['i'].append(float(l.split('=')[1].strip()))
    dat['name'] = filename
    return dat


def get_cir(currents):
    init_average = np.mean(currents[:3])
    return [current / init_average for current in currents]


def get_doses(dt, doserate=0.78):
    startdate = dt[0]
    dts = [(p - startdate).total_seconds() for p in dt]
    return [dt * doserate / 3600 for dt in dts]


def make_data(meta):
    out = []
    for _entry in meta:
        entry = _entry[0]
        if entry.get("name").endswith("_2"):
            continue
        fname1 = "/Users/ric/Documents/DPhil/RAL/irrad_data/2018-08-31/Irradiations/data/{}/Monitoring.txt".format(
            entry["name"])
        _dat1 = get_data(fname1)
        cir1 = get_cir(_dat1['i'])
        doses1 = get_doses(_dat1['dt'])
        if len(cir1) > len(doses1):
            del cir1[-1]
        elif len(doses1) > len(cir1):
            del doses1[-1]

        fname2 = "/Users/ric/Documents/DPhil/RAL/irrad_data/2018-08-31/Irradiations/data/{}_2/Monitoring.txt".format(
            entry["name"])
        _dat2 = get_data(fname2)
        cir2 = get_cir(_dat2['i'])
        doses2 = get_doses(_dat2['dt'])
        if len(cir2) > len(doses2):
            del cir2[-1]
        elif len(doses2) > len(cir2):
            del doses2[-1]
        d = {
            "name": entry["name"],
            "X1": doses1,
            "Y1": cir1,
            "X2": doses2,
            "Y2": cir2,
            "annealing": _entry[1]["annealing"],
            "high T": entry.get("high T")
        }
        out.append(d)

    return out


def makedir(path):
    if not os.path.exists(os.path.dirname(path)):
        os.makedirs(os.path.dirname(path))


def plot_peaks(data):
    fig = plt.figure(figsize=(8, 6))

    ax1 = fig.add_subplot(1, 1, 1)
    ax1.set_xlim(0, 4.1)
    ax1.set_ylim(.9, 8.1)

    ax1.set_xlabel('total ionising dose [Mrad]', fontsize=16)
    ax1.set_ylabel('current-increase ratio', fontsize=16)

    n_high = 0
    n_low = 0
    lownames = []
    for d in data:
        if d["high T"]:
            c = 'm'
            n_high += 1
        else:
            c = 'c'
            lownames.append(d["name"])
            n_low += 1

        ax1.plot(d["X1"], d["Y1"], linestyle='-', color='dimgrey',
                 linewidth=.6)
        ax1.plot(d["X2"], d["Y2"], linestyle='--', color=c, linewidth=.8)

    custom_lines = [Line2D([0], [0], color='dimgrey', lw=2, linestyle='-'),
                    Line2D([0], [0], color='c', lw=2, linestyle='--'),
                    Line2D([0], [0], color='m', lw=2, linestyle='--')]
    print("high", n_high, "low", n_low)
    print(sorted(lownames))
    ax1.legend(custom_lines,
               ['initial irradiation',
                r're-irradiation, cured at $20~^\circ$C',
                r're-irradiation, cured at $80~^\circ$C'],
               loc='upper right', fontsize=12)

    path = 'plots/series/{}_re_irrad.pdf'.format(
        datetime.today().strftime('%Y-%m-%d')
    )

    makedir(path)
    plt.xticks(fontsize=14)
    plt.yticks(fontsize=14)
    fig.savefig(path)
    logger.info('Saved plot under {}'.format(os.path.abspath(path)))


if __name__ == '__main__':
    with io.open('annealing_2.json', 'r') as f:
        metadata = json.loads(f.read())

    data = make_data(metadata)

    plot_peaks(data)
