from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import io
import json
import logging
import os
from datetime import datetime

import matplotlib.pyplot as plt
import numpy as np
from matplotlib.lines import Line2D
from scipy.stats import linregress

plt.style.use('seaborn-paper')

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)


def do():
    with io.open('annealing.json', 'r') as f:
        d1 = json.loads(f.read())
    with io.open('annealing_2.json', 'r') as f:
        d2 = json.loads(f.read())

    _d1 = sorted(e["name"] for e in d1)
    _d2 = sorted(e[0]["name"] for e in d2)
    print(len(_d1), len(_d2))
    for a, b in zip(_d1, _d2):
        if a != b:
            print(a, b)
            break


if __name__ == '__main__':
    do()
