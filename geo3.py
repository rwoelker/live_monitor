from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import logging
import os
from datetime import datetime

import matplotlib as mpl
import matplotlib.cm as cmx
import matplotlib.colors as colors
import matplotlib.pyplot as plt
import numpy as np
from matplotlib import gridspec

plt.style.use('seaborn-dark-palette')

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)

# {chip_no: (r, theta, peak_2 (A), peak_3 (V), peak_4)}
_chips = {
    "005": (1, np.pi / 2., 6.1208, 5.0473, 4.7634),
    "024": (0.8, 3.5 * np.pi / 4., None, 4.9549, None),
    "026": (0.7, 3 * np.pi / 4., 6.4268, None, 4.7643),
    "034": (0.7, np.pi / 4., 7.1019, 5.4604, 4.7722),
    "057": (1, np.pi, 5.9405, 4.1231, 4.5548),
    "065": (0., 0., 7.3217, 5.647, 4.9172),
    "073": (1., 0., 5.5232, 5.9168, 4.7092),
    "096": (0.7, 5 * np.pi / 4, 6.6799, 4.4554, 5.2408),
    "104": (0.7, 7 * np.pi / 4, 6.4586, 5.9955, 5.0507),
    "130": (1, 3 * np.pi / 2, 5.5504, 4.3153, 4.9123),
}

norm_ratio_1 = _chips["065"][2] / _chips["065"][3]
norm_ratio_2 = _chips["065"][2] / _chips["065"][4]

print("norm_1: {} norm_2: {}".format(norm_ratio_1, norm_ratio_2))

peaks_2 = [v[2] for _, v in _chips.items() if v[2] is not None]
peaks_3 = [v[3] for _, v in _chips.items() if v[3] is not None]
peaks_4 = [v[4] for _, v in _chips.items() if v[4] is not None]

for i, a in enumerate((peaks_2, peaks_3, peaks_4)):
    print('peaks {} mean and std ${:.2f}\pm{:.2f}$'.format(i, np.mean(a),
                                                           np.std(a)))

chips = {}
for k, v in _chips.items():
    if v[3] is not None:
        v = (v[0], v[1], v[2], v[3] * norm_ratio_1, v[4])
    if v[4] is not None:
        v = (v[0], v[1], v[2], v[3], v[4] * norm_ratio_2)
    chips[k] = v


def trans_polar(r=0, theta=0, x0=0, y0=0):
    x = r * np.cos(theta)
    y = r * np.sin(theta)
    x1 = x + x0
    y1 = y + y0
    r1 = np.sqrt(x1 ** 2 + y1 ** 2)
    t1 = np.arccos((x + x0) / r1)
    if theta >= np.pi:
        t1 = -t1
    return t1, r1


def makedir(path):
    if not os.path.exists(os.path.dirname(path)):
        os.makedirs(os.path.dirname(path))


def plot_geo():
    fig = plt.figure(figsize=(12, 6))

    gs1 = gridspec.GridSpec(2, 3, height_ratios=[20, 1])
    # gs1.update(left=0.05, right=0.48, wspace=0.05)

    ax2 = plt.subplot(gs1[1, :])
    ax1 = plt.subplot(gs1[0, 0], projection='polar')
    ax3 = plt.subplot(gs1[0, 1], projection='polar')
    ax4 = plt.subplot(gs1[0, 2], projection='polar')

    ax1.set_title('Batch 2: A9Q8IIH', fontsize=20)
    ax3.set_title('Batch 3: VXCQ20H', fontsize=20)
    ax4.set_title('Batch 3: VCCQ70H', fontsize=20)

    for a in (ax1, ax3, ax4):
        a.grid(False)
        a.set_rticks([])
        a.set_xticks([])
        a.set_rmax(1)
        a.set_ylim(0, 1.15)
        a.set_facecolor('gainsboro')

    vmin = np.floor(np.amin(peaks_2 + peaks_3 + peaks_4))
    vmax = np.ceil(np.amax(peaks_2 + peaks_3 + peaks_4))

    hot = cm = mpl.cm.viridis
    cNorm = colors.Normalize(vmin=vmin - 1, vmax=vmax)
    scalarMap = cmx.ScalarMappable(norm=cNorm, cmap=hot)

    for k, v in chips.items():
        if v[2] is not None:
            ax1.scatter(v[1], v[0], marker='s', s=500,
                        color=scalarMap.to_rgba(v[2]))
            ax1.annotate(k, trans_polar(r=v[0], theta=v[1], x0=-0.05))

        if v[3] is not None:
            ax3.scatter(v[1], v[0], marker='s', s=500,
                        color=scalarMap.to_rgba(v[3]))
            ax3.annotate(k, trans_polar(r=v[0], theta=v[1], x0=-0.05))

        if v[4] is not None:
            label = k
            if k == "005":
                label = "003"
            ax4.scatter(v[1], v[0], marker='s', s=500,
                        color=scalarMap.to_rgba(v[4]))
            ax4.annotate(label, trans_polar(r=v[0], theta=v[1], x0=-0.05))

    ax2.set_position([0.3, 0.12, 0.4, 0.015])
    cb1 = mpl.colorbar.ColorbarBase(ax2, cmap=hot,
                                    norm=cNorm,
                                    orientation='horizontal')
    cb1.ax.tick_params(labelsize=12)
    cb1.set_label('maximum current-increase ratio', labelpad=None, fontsize=20)

    path = 'plots/geographical/{}_geographical.pdf'.format(
        datetime.today().strftime('%Y-%m-%d'))

    makedir(path)
    fig.savefig(path, bbox_inches='tight')
    logger.info('Saved plot under {}'.format(os.path.abspath(path)))


if __name__ == '__main__':
    plot_geo()
