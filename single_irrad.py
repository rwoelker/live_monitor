from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import io
import logging
import os
from collections import defaultdict
from datetime import datetime
from scipy.optimize import curve_fit

import matplotlib.pyplot as plt
import numpy as np
from matplotlib.lines import Line2D

plt.style.use('seaborn-dark-palette')
plt.rcParams['font.family'] = 'serif'
plt.rcParams['font.serif'] = 'Ubuntu'
plt.rcParams['font.monospace'] = 'Ubuntu Mono'
plt.rcParams['font.size'] = 10
plt.rcParams['axes.labelsize'] = 10
plt.rcParams['axes.labelweight'] = 'bold'
plt.rcParams['xtick.labelsize'] = 8
plt.rcParams['ytick.labelsize'] = 8
plt.rcParams['legend.fontsize'] = 10
plt.rcParams['figure.titlesize'] = 12

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)


def makedir(path):
    if not os.path.exists(os.path.dirname(path)):
        os.makedirs(os.path.dirname(path))


def get_data(filename):
    dat = defaultdict(list)
    with io.open(filename, 'r') as f:
        for index, l in enumerate(f):
            if l.startswith('==='):
                dt_string = l.split('===')[1].strip()
                dt_obj = datetime.strptime(dt_string, '%d/%m/%Y %H:%M.%S')
                dat['dt'].append(dt_obj)
            elif l.startswith('NTC'):
                dat['t'].append(float(l.split('=')[1].strip()))
            elif l.startswith('IDDD'):
                dat['i'].append(float(l.split('=')[1].strip()))
            elif l.startswith('VDDA_RAW'):
                dat['vdda'].append(float(l.split('=')[2].strip()))
            elif l.startswith('VDDD_RAW'):
                dat['vddd'].append(float(l.split('=')[2].strip()))
    dat['name'] = filename
    return dat


def func(x, *args):
    return 1.5 + args[0] / (x + args[1])


def plot_dose(files):
    custom_lines = [Line2D([0], [0], color='b', lw=4),
                    Line2D([0], [0], color='k', lw=4)]
    fig = plt.figure(figsize=(6, 4))
    ax = fig.add_subplot(1, 1, 1)
    ax.set_ylabel(r'current-increase ratio')
    ax.set_xlabel(r'time [a.u.]')
    ax.set_xlim(0, 2)
    ax.set_ylim(1, 6)

    dat = get_data(files[0])
    start = dat['i'][0]
    startdate = dat['dt'][0]
    dts = [(p - startdate).total_seconds() for p in dat['dt']]
    X_1 = np.array(dts) * 0.78 / 3600
    Y_1 = np.array(dat['i']) / start

    ax.plot(X_1, Y_1, color='k', linewidth=2)

    dat_2 = get_data(files[1])
    enddate_1 = dat['dt'][-1]
    startdate_2 = dat_2['dt'][0]
    second_shift = (startdate_2 - enddate_1).total_seconds()
    dts = [(p - startdate_2).total_seconds() + second_shift for p in
           dat_2['dt']]
    X_2 = X_1[-1] + np.array(dts) * 0.78 / 3600
    Y_2 = np.array(dat_2['i']) / start
    ax.plot(X_2, Y_2, color='k', linewidth=2)

    # now fit the decaying exponential
    # first point should be end of first plot
    X_fit = list(X_1[-1:]) + list(X_2)
    Y_fit = list(Y_1[-1:]) + list(Y_2)
    popt, _ = curve_fit(lambda x, a, b, c: func(x, a, b, c), X_fit, Y_fit,
                        maxfev=10000)

    # plot that thing
    Xplot = np.linspace(X_1[-1], 2, 100)
    args = tuple(p for p in popt)
    Yplot = func(Xplot, *args)
    ax.plot(Xplot, Yplot, color='k', linewidth=1, linestyle='--')

    ax.text(1.18, 5, r'fit: $y= 1.5 + {a:.2f} / (x {b:.2f})$'.format(a=popt[0],
                                                                     b=popt[1],
                                                                     fontsize=8))

    path = os.path.join(
        'plots/exp_decay/{}/'.format(datetime.today().strftime('%Y-%m-%d')),
        'exp_decay.pdf')
    makedir(path)
    fig.savefig(path)
    logger.info('Saved plot under {}'.format(path))


if __name__ == '__main__':
    files = [
        '/Users/ric/Documents/DPhil/RAL/irrad_data/2018-07-27/Monitoring_1.txt',
        '/Users/ric/Documents/DPhil/RAL/irrad_data/2018-07-27/Monitoring_2.txt'
    ]
    plot_dose(files)
