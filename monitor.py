from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import argparse
import io
import logging
import math
import matplotlib.pyplot as plt
import time
from collections import defaultdict
from datetime import datetime

plt.style.use('seaborn-dark-palette')
plt.rcParams['font.family'] = 'serif'
plt.rcParams['font.serif'] = 'Ubuntu'
plt.rcParams['font.monospace'] = 'Ubuntu Mono'
plt.rcParams['font.size'] = 10
plt.rcParams['axes.labelsize'] = 10
plt.rcParams['axes.labelweight'] = 'bold'
plt.rcParams['xtick.labelsize'] = 8
plt.rcParams['ytick.labelsize'] = 8
plt.rcParams['legend.fontsize'] = 10
plt.rcParams['figure.titlesize'] = 12

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)


def create_argument_parser():
    parser = argparse.ArgumentParser(
        description='Live display for ABC130 irradiations')
    parser.add_argument(
        '-f', '--file',
        type=str,
        default='Monitoring.txt',
        help='Filename to scan')
    parser.add_argument(
        '-i', '--interval',
        type=float,
        default=15,
        help='Refresh interval in seconds')
    parser.add_argument(
        '-d', '--doserate',
        type=float,
        default=0.78,
        help='Dose rate in Mrad/h')
    parser.add_argument(
        '-l', '--length',
        type=float,
        default=5,
        help='Length of the irradiation in h')
    return parser


def update_data(filename, dat):
    with io.open(filename, 'r') as f:
        skip = False
        for index, l in enumerate(f):
            if l.startswith('==='):
                dt_string = l.split('===')[1].strip()
                dt_obj = datetime.strptime(dt_string, '%d/%m/%Y %H:%M.%S')
                skip = True if dt_obj in dat['dt'] else False
                if not skip:
                    dat['dt'].append(dt_obj)
            elif l.startswith('NTC') and not skip:
                dat['t'].append(float(l.split('=')[1].strip()))
            elif l.startswith('IDDD') and not skip:
                dat['i'].append(float(l.split('=')[1].strip()))
            elif l.startswith('VDDA_RAW') and not skip:
                dat['vdda'].append(float(l.split('=')[2].strip()))
            elif l.startswith('VDDD_RAW') and not skip:
                dat['vddd'].append(float(l.split('=')[2].strip()))


def draw_canvas(figure, dat, doserate):
    try:
        figure.canvas.draw()
        figure.canvas.flush_events()
    except ValueError as e:  # redraw everything on exception
        logging.error('Error while drawing canvas: {}'.format(e))
        truncate_lists(dat)
        update_view(figure, dat, doserate)


def truncate_lists(dic):
    '''Truncates all lists in a dictionary to the length of the shortest one'''
    min_length = min(map(len, dic.values()))
    for k in dic:
        while len(dic[
                      k]) > min_length:  # delete last element of list until min_length is reached
            del dic[k][-1]


def update_view(figure, dat, doserate):
    start_time = dat['dt'][0]

    doses = [(d - start_time).total_seconds() * doserate / 3600 for d in
             dat['dt']]
    times = [(d - start_time).total_seconds() / 3600 for d in dat['dt']]

    ax1, ax2, ax3, ax4 = figure.axes

    line1 = ax1.get_lines()[0]
    line2 = ax2.get_lines()[0]
    line3 = ax3.get_lines()[0]
    line4 = ax4.get_lines()[0]

    line1.set_xdata(doses)
    line1.set_ydata(dat['i'])

    line2.set_xdata(times)
    line2.set_ydata(dat['t'])

    line3.set_xdata(times)
    line3.set_ydata(dat['vddd'])

    line4.set_xdata(times)
    line4.set_ydata(dat['vdda'])

    ax1.set_xlim(0, max((max(doses) * 1.1, 0.1)))
    ax1.set_ylim(0, max(dat['i']) * 1.1)

    ax2.set_xlim(0, max(max(times) * 1.1, .1))
    ax2.set_ylim(10, max(dat['t']) * 1.2)

    ax3.set_xlim(0, max(max(times) * 1.1, .1))
    ax3.set_ylim(500, max(dat['vddd']) * 1.2)

    ax4.set_xlim(0, max(max(times) * 1.1, .1))
    ax4.set_ylim(500, max(dat['vdda']) * 1.2)

    draw_canvas(figure, dat, doserate)


def monitoring(filename, interval, doserate, length):
    data = defaultdict(list)

    plt.ion()

    fig = plt.figure(figsize=(12, 7))

    ax1, ax2, ax3, ax4 = [fig.add_subplot(n) for n in range(221, 225)]

    ax1.set_xlabel('TID [Mrad]')
    ax1.set_ylabel('IDDD [mA]')

    ax2.set_xlabel('time [h]')
    ax2.set_ylabel('T [C]')

    ax3.set_xlabel('time [h]')
    ax3.set_ylabel('VDDD [mV]')

    ax4.set_xlabel('time [h]')
    ax4.set_ylabel('VDDA [mV]')

    map(lambda x: x.plot([], [], linewidth=2), fig.axes)

    for _ in range(int(math.ceil(length * 3600. / interval))):
        update_data(filename, data)
        update_view(fig, data, doserate)
        start_time = data['dt'][0]
        dose = (data['dt'][-1] - start_time).total_seconds() * doserate / 3600
        t_ = dose * 3600 / doserate
        logger.info('Plotting {} data points'.format(len(data['dt'])))
        logger.info(
            'latest dose: {:.3g} Mrad --- latest current: {:.3g} mA --- latest temperature: {:.1f} C'.format(
                dose, data['i'][-1], data['t'][-1]))
        logger.info('Time since start: {} s'.format(t_))
        logger.info('=' * 86)
        time.sleep(interval)


if __name__ == '__main__':
    args = create_argument_parser().parse_args()
    logging.info(
        'Starting current monitor for file {} -- interval {} s --- dose rate {} Mrad/h --- length {} h'.format(
            args.file, args.interval, args.doserate, args.length
        ))
    logging.info('#' * 100 + '\n')
    monitoring(args.file, args.interval, args.doserate, args.length)
