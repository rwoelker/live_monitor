from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import logging
import os
from datetime import datetime

import matplotlib.pyplot as plt
from matplotlib.lines import Line2D
from scipy.stats import linregress

plt.style.use('seaborn-dark-palette')

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)

peaks = [[{"name": "AYQ2XTH_077", "peak": 5.33056244254},

          {"name": "AYQ2XTH_077_2", "peak": 1.19899981022, "annealing": 259}],
         [{"name": "VFCQ8EH_065", "peak": 5.22866210937},
          {"name": "VFCQ8EH_065_2", "peak": 1.31002035737, "annealing": 49}],
         [{"name": "AGQ8DWH_028", "peak": 6.64160175323},
          {"name": "AGQ8DWH_028_2", "peak": 1.31766687036, "annealing": 291}],
         [{"name": "AGQ8DWH_027", "peak": 6.400},
          {"name": "AGQ8DWH_027_2", "peak": 1.327, "annealing": 273}],
         [{"name": "A7Q8IKH_065", "peak": 6.95931077003},
          {"name": "A7Q8IKH_065_2", "peak": 1.26585091352, "annealing": 50}],
         [{"name": "A6Q2XKH_089", "peak": 4.93206076622},
          {"name": "A6Q2XKH_089_2", "peak": 1.21509184837, "annealing": 302}],
         [{"name": "VNCQ67H_065", "peak": 5.37146679759},
          {"name": "VNCQ67H_065_2", "peak": 1.32149916887, "annealing": 48}],
         [{"name": "VPCQ66H_063", "peak": 5.29407230616},
          {"name": "VPCQ66H_063_2", "peak": 1.34516371489, "annealing": 52}],
         [{"name": "A4Q8INH_133", "peak": 5.41463740468},
          {"name": "A4Q8INH_133_2", "peak": 1.24458395243, "annealing": 286}],
         [{"name": "VJCQ7TH_065", "peak": 6.3750022471},
          {"name": "VJCQ7TH_065_2", "peak": 1.45987131596, "annealing": 53}],
         [{"name": "AYQ2XTH_018", "peak": 4.82611122131},
          {"name": "AYQ2XTH_018_2", "peak": 1.21956692934, "annealing": 285}],
         [{"name": "A6Q2XKH_134", "peak": 3.8222145617},
          {"name": "A6Q2XKH_134_2", "peak": 1.17526143789, "annealing": 309}],
         [{"name": "VCCQ70H_057", "peak": 4.5549},
          {"name": "VCCQ70H_057_2", "peak": 1.2491, "annealing": 5}],
         [{"name": "VCCQ70H_104", "peak": 5.0575},
          {"name": "VCCQ70H_104_2", "peak": 2.0908, "annealing": 25}],
         [{"name": "VCCQ70H_065", "peak": 4.9173},
          {"name": "VCCQ70H_065_2", "peak": 1.3018, "annealing": 8}],
         [{"name": "VCCQ70H_073", "peak": 4.7092},
          {"name": "VCCQ70H_073_2", "peak": 1.2795, "annealing": 7}],
         [{"name": "VCCQ70H_130", "peak": 4.9124},
          {"name": "VCCQ70H_130_2", "peak": 1.3571, "annealing": 30}],
         [{"name": "VCCQ70H_096", "peak": 5.24087556601},
          {"name": "VCCQ70H_096_2", "peak": 1.29259706736, "annealing": 11}],
         [{"name": "VCCQ70H_034", "peak": 4.7722},
          {"name": "VCCQ70H_034_2", "peak": 2.1688, "annealing": 34}],
         [{"name": "VCCQ70H_096", "peak": 4.7643},
          {"name": "VCCQ70H_096_2", "peak": 1.295, "annealing": 13}],
         [{"name": "A6Q2XKH_065", "peak": 5.37798690796, "high T": True},
          {"name": "A6Q2XKH_065_2", "peak": 1.61004297137, "annealing": 60}],
         [{"name": "ADQ81EH_065", "peak": 7.61178235412, "high T": True},
          {"name": "ADQ81EH_065_2", "peak": 1.46072169542, "annealing": 60}],
         [{"name": "A9Q8IIH_065", "peak": 7.95385069847, "high T": True},
          {"name": "A9Q8IIH_065_2", "peak": 1.54684350491, "annealing": 60}],
         [{"name": "VXCQ20H_065", "peak": 6.11228199601, "high T": True},
          {"name": "VXCQ20H_065_2", "peak": 1.27792669535, "annealing": 60}],
         [{"name": "VRCQ26H_065", "peak": 5.80419317484, "high T": True},
          {"name": "VRCQ26H_065_2", "peak": 1.25094481707, "annealing": 60}],
         [{"name": "VSCQ63H_065", "peak": 4.89491761923, "high T": True},
          {"name": "VSCQ63H_065_2", "peak": 1.31413804293, "annealing": 60}],
         [{"name": "VTCQ24H_065", "peak": 5.43267, "high T": True},
          {"name": "VTCQ24H_065_2", "peak": 1.27412, "annealing": 120}],
         [{"name": "ASQ8E2H_065", "peak": 7.34385, "high T": True},
          {"name": "ASQ8E2H_065_2", "peak": 1.41436, "annealing": 120}],
         [{"name": "VXCQ20H_057", "peak": 4.3544, "high T": True},
          {"name": "VXCQ20H_057_2", "peak": 1.2047, "annealing": 120}],
         [{"name": "A9Q8IIH_057", "peak": 6.4345, "high T": True},
          {"name": "A9Q8IIH_057_2", "peak": 1.3479, "annealing": 120}],
         ]


def makedir(path):
    if not os.path.exists(os.path.dirname(path)):
        os.makedirs(os.path.dirname(path))


def plot_peaks():
    fig = plt.figure(figsize=(5, 4))

    ax1 = fig.add_subplot(1, 1, 1)
    ax1.set_xlim(0, 320)
    ax1.set_ylim(0, .5)

    ax1.set_xlabel('curing time [d]', fontsize=18)
    ax1.set_ylabel('relative maximum current-increase ratio', fontsize=18)
    X = []
    Y = []
    custom_patches = [
        Line2D([0], [0], color='w', marker='^', markerfacecolor='k',
               markersize=10),
        Line2D([0], [0], color='w', marker='X', markerfacecolor='r',
               markersize=10)]
    ax1.legend(custom_patches, [r'$20~^\circ$C', r'$80~^\circ$C'],
               loc="upper right")
    for i, dat in enumerate(peaks):
        x = dat[1]['annealing']
        X.append(x)
        y = dat[1]['peak'] / dat[0]['peak']
        Y.append(y)
        marker = 'X' if dat[0].get('high T') else '^'
        color = 'r' if dat[0].get('high T') else 'k'
        s = 80 if dat[0].get('high T') else 80
        ax1.scatter(x, y, c=color, marker=marker, s=s)

    slope, intercept, r_value, p_value, std_err = linregress(X, Y)
    logger.info('slope: {:.3g}, intercept: {:.3g}, '
                'r_value: {:.3g}, p_value: {:.3g}, '
                'std_err: {:.3g}'.format(slope, intercept, r_value, p_value,
                                         std_err))

    path = 'plots/re_irradiations/{}_peak_ratios.pdf'.format(
        datetime.today().strftime('%Y-%m-%d'))
    makedir(path)
    fig.savefig(path, bbox_inches='tight')
    logger.info('Saved plot under {}'.format(path))


if __name__ == '__main__':
    plot_peaks()
