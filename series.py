import io
import json
import logging
import os
from collections import defaultdict
from datetime import datetime

import matplotlib.pyplot as plt
import numpy as np
from matplotlib.lines import Line2D

plt.style.use('seaborn-paper')

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)


def get_data(filename):
    dat = defaultdict(list)
    with io.open(filename, 'r') as f:
        for index, l in enumerate(f):
            if l.startswith('==='):
                dt_string = l.split('===')[1].strip()
                dt_obj = datetime.strptime(dt_string, '%d/%m/%Y %H:%M.%S')
                dat['dt'].append(dt_obj)
            elif l.startswith('IDDD'):
                dat['i'].append(float(l.split('=')[1].strip()))
    dat['name'] = filename
    return dat


def get_cir(currents):
    init_average = np.mean(currents[:3])
    return [current / init_average for current in currents]


def get_doses(dt, doserate=0.78):
    startdate = dt[0]
    dts = [(p - startdate).total_seconds() for p in dt]
    return [dt * doserate / 3600 for dt in dts]


def make_data(meta):
    out = []
    for entry in meta:
        fname = "/Users/ric/Documents/DPhil/RAL/irrad_data/2018-08-31/Irradiations/data/{}/Monitoring.txt".format(
            entry["name"])
        _dat = get_data(fname)
        cir = get_cir(_dat['i'])
        doses = get_doses(_dat['dt'])
        d = {
            "name": entry["name"],
            "X": doses,
            "Y": cir,
            "batch": entry["batch"]
        }
        out.append(d)

    return out


def makedir(path):
    if not os.path.exists(os.path.dirname(path)):
        os.makedirs(os.path.dirname(path))


def plot_peaks(data):
    fig = plt.figure(figsize=(8, 6))

    ax1 = fig.add_subplot(1, 1, 1)
    ax1.set_xlim(0, 4.1)
    ax1.set_ylim(.9, 8.1)

    ax1.set_xlabel('total ionising dose [Mrad]', fontsize=16)
    ax1.set_ylabel('current-increase ratio', fontsize=16)

    for d in data:
        if d["batch"] == 2:
            linestyle = "-"
            c = 'dimgrey'
            lw = .6
        elif d["batch"] == 3:
            linestyle = "-"
            c = 'c'
            lw = .6
        else:
            continue

        ax1.plot(d["X"], d["Y"], linestyle=linestyle, color=c, linewidth=lw)

    for d in data:
        if d["batch"] == 1:
            linestyle = "-"
            c = 'm'
            lw = 1.5
        else:
            continue

        ax1.plot(d["X"], d["Y"], linestyle=linestyle, color=c, linewidth=lw)

    custom_lines = [Line2D([0], [0], color='dimgrey', lw=2, linestyle='-'),
                    Line2D([0], [0], color='c', lw=2, linestyle='-'),
                    Line2D([0], [0], color='m', lw=3, linestyle='-')]

    ax1.legend(custom_lines,
               ['batch 2', 'batch 3', 'batch 1'],
               loc='upper right', fontsize=14)

    path = 'plots/series/{}_geo.pdf'.format(
        datetime.today().strftime('%Y-%m-%d')
    )

    makedir(path)
    plt.xticks(fontsize=14)
    plt.yticks(fontsize=14)
    fig.savefig(path)
    logger.info('Saved plot under {}'.format(os.path.abspath(path)))


if __name__ == '__main__':
    with io.open('raw_dat_mid_wafer.json', 'r') as f:
        metadata = json.loads(f.read())

    data = make_data(metadata)

    plot_peaks(data)
