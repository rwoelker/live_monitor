import io
import json
import logging
import os
from collections import defaultdict
from datetime import datetime

import matplotlib.pyplot as plt
import numpy as np

plt.style.use('seaborn-paper')

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)


def get_data(filename):
    dat = defaultdict(list)
    with io.open(filename, 'r') as f:
        for index, l in enumerate(f):
            if l.startswith('==='):
                dt_string = l.split('===')[1].strip()
                dt_obj = datetime.strptime(dt_string, '%d/%m/%Y %H:%M.%S')
                dat['dt'].append(dt_obj)
            elif l.startswith('IDDD'):
                dat['i'].append(float(l.split('=')[1].strip()))
    dat['name'] = filename
    return dat


def get_cir(currents):
    init_average = np.mean(currents[:3])
    return [current / init_average for current in currents]


def get_doses(dt, doserate=0.78):
    startdate = dt[0]
    dts = [(p - startdate).total_seconds() for p in dt]
    return [dt * doserate / 3600 for dt in dts]


def make_data(meta):
    out = []
    for entry in meta:
        _dat = get_data(entry["file"])
        cir = get_cir(_dat['i'])
        doses = get_doses(_dat['dt'])
        d = {
            "name": entry["name"],
            "X": doses,
            "Y": cir
        }
        out.append(d)

    return out


def makedir(path):
    if not os.path.exists(os.path.dirname(path)):
        os.makedirs(os.path.dirname(path))


def plot_peaks(data):
    fig = plt.figure(figsize=(8, 6))

    ax1 = fig.add_subplot(1, 1, 1)
    ax1.set_xlim(0, 4.1)
    ax1.set_ylim(.9, 5.5)

    ax1.set_xlabel('total ionising dose [Mrad]', fontsize=16)
    ax1.set_ylabel('current-increase ratio', fontsize=16)

    x1 = data[0]["X"]  # taiwan
    y1 = data[0]["Y"]

    x2 = data[1]["X"]  # ral
    y2 = data[1]["Y"]

    x3 = data[2]["X"]  # ral2
    y3 = data[2]["Y"]

    ax1.plot(x2, y2, label='VTCQ24H\_065', color='dimgrey')
    ax1.plot(x1, y1, label='VTCQ24H\_067, pre-irradiated 8 Mrad', color='c',
             linestyle='--')
    ax1.plot(x3, y3, label='VTCQ24H\_005, pre-irradiated 8 Mrad', color='m',
             linestyle='--')

    ax1.legend(loc='upper right', fontsize=12)

    path = 'plots/taiwan/{}_comparison_2.pdf'.format(
        datetime.today().strftime('%Y-%m-%d')
    )

    makedir(path)
    plt.xticks(fontsize=14)
    plt.yticks(fontsize=14)
    fig.savefig(path)
    logger.info('Saved plot under {}'.format(path))


if __name__ == '__main__':
    with io.open('raw_dat.json', 'r') as f:
        metadata = json.loads(f.read())

    data = make_data(metadata)

    plot_peaks(data)
